import Link from 'next/link'

const links = [
  {
    url: '/three_sample',
    title: 'three_sample'
  },
  {
    url: '/three_sample_auto_size',
    title: 'three_sample_auto_size'
  },
  {
    url: '/three_sample_auto_size_custom_model',
    title: 'three_sample_auto_size_custom_model'
  },
  {
    url: '/three_sample_zoom',
    title: 'three_sample_zoom'
  },
  {
    url: '/three_sample_mouse_pointer_lock_rotation',
    title: 'three_sample_mouse_pointer_lock_rotation'
  },
]

export default function Home() {
  return (
    <main>
      <div>
        <ul className="pt-2 pl-2">
          {
            links.map((link, idx) => {
              return (
                <li key={link.title}>
                  <Link href={link.url}>
                  {idx + 1}) {link.title}
                  </Link>
                </li>                
              )
            })
          }
        </ul>
      </div>
    </main>
  )
}

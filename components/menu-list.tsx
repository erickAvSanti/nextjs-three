import Link from "next/link";

const MenuList = () => {
  return (
    <Link href="/" className="inline-flex absolute">
      <div style={ {color: 'white'} }>Home</div>
    </Link>
  );
}

export default MenuList;
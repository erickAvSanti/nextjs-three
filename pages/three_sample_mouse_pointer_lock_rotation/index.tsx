import { useEffect, useRef, useState } from "react";
import { setupThree } from "./lib";
import MenuList from "@/components/menu-list";

const Test = () => {
  const canvas1 = useRef<HTMLCanvasElement | null>(null);
  const [threeProp, setThreeProp] = useState<any>(null);
  useEffect(() => {
    if(canvas1.current == null) return;
    const threeProp = setupThree(canvas1.current);
    setThreeProp(threeProp);
    return () => {
      threeProp.clear();
    }
  }, []);
  return (
    <>
      <MenuList></MenuList>
      <canvas ref={canvas1} onClick={() => threeProp.lockContainer()}></canvas>
    </>
  );
}

export default Test;
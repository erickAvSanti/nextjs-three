import * as THREE from 'three';
import { GLTFLoader } from 'three/addons/loaders/GLTFLoader.js';
export const setupThree = (canvasElement: any) => {
  const scene = new THREE.Scene();
  const camera = new THREE.PerspectiveCamera( 75, window.innerWidth / window.innerHeight, 0.1, 1000 );
  
  const renderer = new THREE.WebGLRenderer({ canvas: canvasElement });
  renderer.setPixelRatio(window.devicePixelRatio);
  renderer.setSize( window.innerWidth, window.innerHeight );
  
  camera.position.z = 5;

  let customModel: any = null;

  let animFrameId = 0;
  
  function animate() {
    animFrameId = requestAnimationFrame( animate );

    if(customModel != null) {
  
      customModel.rotation.x += 0.01;
      customModel.rotation.y += 0.01;

    }
  
    renderer.render( scene, camera );
  }
  
  animate();

  window.addEventListener('resize', (evt) => {
    camera.aspect = window.innerWidth / window.innerHeight;
    camera.updateProjectionMatrix();
    renderer.setSize( window.innerWidth, window.innerHeight );
  });

  const light = new THREE.AmbientLight( 0x404040 ); // soft white light
  scene.add( light );

  const directionalLight = new THREE.DirectionalLight( 0xffffff, 0.5 );
  scene.add( directionalLight );

  const loader = new GLTFLoader();
  loader.load( '/three/model1.glb', function ( gltf ) {

    customModel = gltf.scene;
    scene.add( customModel );
  });

  return {
    clear() {
      cancelAnimationFrame(animFrameId);
    }
  }
};
import { useEffect, useRef } from "react";
import { setupThree } from "./lib";
import MenuList from "@/components/menu-list";

const Test = () => {
  const canvas1 = useRef(null);
  useEffect(() => {
    const threeProp = setupThree(canvas1.current);
    return () => {
      threeProp.clear();
    }
  }, []);
  return (
    <>
      <MenuList></MenuList>
      <canvas ref={canvas1}></canvas>
    </>
  );
}

export default Test;
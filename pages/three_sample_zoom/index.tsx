import { useEffect, useRef, useState } from "react";
import { setupThree } from "./lib";
import MenuList from "@/components/menu-list";

const Test = () => {
  const canvas1 = useRef(null);
  const [threeProp, setThreeProp] = useState<any>(null);
  useEffect(() => {
    const threeProp = setupThree(canvas1.current);
    setThreeProp(threeProp);
    return () => {
      threeProp.clear();
    }
  }, []);
  return (
    <>
      <MenuList></MenuList>
      <canvas ref={canvas1} onWheel={(event) => threeProp.zoomWheel(event)}></canvas>
      <div className="absolute bottom-0 z-10 mx-auto">
        <div className="buttons">
          <button className="text-white px-1 bg-orange-800 mx-0.5 w-13 rounded" onClick={() => threeProp.zoomIn()}>Zoom In</button>
          <button className="text-white px-1 bg-orange-800 mx-0.5 w-13 rounded" onClick={() => threeProp.zoomOut()}>Zoom Out</button>
        </div>
      </div>
    </>
  );
}

export default Test;